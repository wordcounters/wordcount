#### Phase 1

So far, the simplest approach (as a team) seems to be to have each group member work on a different function of the program.

To elaborate, we will have a team member working on each of the following tasks:

* Create a function for reading the number of lines      (Jessica) COMPLETE
* Create a function for reading the number of words      (Sadia)
* Create a function for reading the number of characters (Briant)  COMPLETE

I (Norbu) will modify the code of the other team members so that they integrate well into the main project.

#### Phase 2

After the above three tasks are finished, we can meet and discuss how we will tackle the final two tasks:

* Create a function for detecting the number of *unique* lines
* Create a function for detecting the number of *unique* words

#### Testing Phase

We will run the `test.sh` script provided by Prof. Skeith. According to him, it will not tell us if our program is foolproof perfect. 

Thus, we will have another meeting to find any problems with our project.

