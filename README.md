## Synopsis

This project was assigned to our team as part of the course, *CSC 103: Intro to Computing*, taught by Prof. William E. Skeith.

Our goal is to implement a tool similar to Unix' `wc` tool.

## Meeting FIRST

Before any one of us writes any lines of code, we will have a team meeting to discuss the outline of how we will work on our project.

I will, however, include files from Prof. Skeith's projects repository. 

## [Plan](PLAN.md)


## Contributors

* Norbu Tsering
* Jessica Shim
* Briant Illescas
* Sadia Sakandar