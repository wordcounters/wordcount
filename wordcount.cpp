/*
 * CSc103 Project 3: wordcount++
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 * http://www.cplusplus.com/reference/vector/vector/
 * http://stackoverflow.com/a/20052728
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 * Norbu: 12
 * Sadia: 4
 * Jessica: 8
 * Briant: 8
 */


#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;
#include <vector>
using std::vector;


/* Some "Helper" Functions */
void            vector_to_set(vector<string> &v, set<string> &s);
bool            is_space(char c);
vector<string>  get_lines_from_input();
vector<string>  get_vector_of_words(string line);


unsigned long count_words(vector<string> string_vector);
unsigned long count_words(vector<string> string_vector);
unsigned long count_chars(vector<string> string_vector);
unsigned long count_lines(vector<string> string_vector);
unsigned long count_unique_words(vector<string> string_vector);
unsigned long count_unique_lines(vector<string> string_vector);

void do_project();


int main()
{
    do_project();
    return 0;
}


vector<string> get_lines_from_input()
{
    vector<string> string_vector;
    string line;

    while (getline(cin, line))
    {
        string_vector.push_back(line);
    }
    return string_vector;
}

/* Converts vectors to sets */
void vector_to_set(vector<string> &v, set<string> &s)
{
    set<string> set_tmp(v.begin(), v.end());
    s = set_tmp;
}


bool is_space(char c)
{
    if (c == ' ' || c == '\t')
        return true;
    return false;
}

vector<string> get_vector_of_words(string line)
{
    /*
     * Finite state machine approach
     */
    bool reading_white_state = true;
    vector<string> output;

    for (unsigned int i = 0; i < line.length(); i++)
    {
        char c = line[i];

        // If we encounter the first character of a word or gap
        if (reading_white_state != is_space(c))
        {
            // Toggle the state
            reading_white_state = !reading_white_state;

            // if we went from whitespace to non-whitespace
            if (!reading_white_state)
            {
                // push_back an empty string to the output vector
                output.push_back("");
            }
        }

        // if we went from whitespace to non-whitespace
        if (!reading_white_state)
        {
            output.back() += c;
        }
    }

    return output;
}


unsigned long count_words(vector<string> string_vector)
{
    unsigned long cw = 0;
    for (unsigned int i = 0; i < string_vector.size(); i++)
    {
        string line = string_vector[i];

        cw += get_vector_of_words(line).size();
    }

    return cw;
}

unsigned long count_chars(vector<string> string_vector)
{
    unsigned long cc = 0;
    for (unsigned int i = 0; i < string_vector.size(); i++)
    {
        string c = string_vector[i];
        cc += c.size() + 1; // + 1 to account for new lines

    }
    return cc;
}

unsigned long count_lines(vector<string> string_vector)
{
    return string_vector.size();
}

/* For this function, flatten the vector of lines into one long vector 
 * of words. Then, insert each word into a set. Finally, you can count 
 * the number of elements in the set.
 */
unsigned long count_unique_words(vector<string> string_vector)
{
    vector<string> uw;
    for (unsigned int i = 0; i < string_vector.size(); i++)
    {
        vector<string> vec_of_words = get_vector_of_words(string_vector[i]);
        for (unsigned int j = 0; j < vec_of_words.size(); j++)
        {
            uw.push_back(vec_of_words[j]);
        }
    }

    set<string> output;
    vector_to_set(uw, output); // convert vector to set
    return output.size();// return .size() of set
}

/* For this function, I think we can convert the vector
 * into a set. Then, count the number of elements in the set.
 */
unsigned long count_unique_lines(vector<string> string_vector)
{
	set<string>  temp_set;
	vector_to_set(string_vector, temp_set);
	int num_lines = temp_set.size();
	return num_lines;
}


void do_project()
{
    vector<string> string_vector = get_lines_from_input();

    unsigned long num_lines        = count_lines(string_vector);
    unsigned long num_words        = count_words(string_vector);
    unsigned long num_chars        = count_chars(string_vector);
    unsigned long num_unique_lines = count_unique_lines(string_vector);
    unsigned long num_unique_words = count_unique_words(string_vector);

    cout << num_lines        << "\t";
    cout << num_words        << "\t";
    cout << num_chars        << "\t";
    cout << num_unique_lines << "\t";
    cout << num_unique_words << "\t";
    cout << endl;
}
